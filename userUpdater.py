#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import json
import sys
import re

#################################GLOBAL VARIABLES##################################
###################################################################################

#Zendesk account information
username = ''
pwd = ''
#Zendesk base url - example: 'https://ticketsupport1.zendesk.com'
BaseURL= ''

####################################################################################
####################################################################################

def getUserIdByEmail(email):
    """
    Finds user Id by input email
    Returns Id if it's found or empty string if it's not found
    """
    global username
    global pwd
    global BaseURL
    url = BaseURL+'/api/v2/users/search.json?query='+email
    result = ''

    response = requests.get(url, auth=(username, pwd))
    if response.status_code == 401:
        print('Status: %s Unauthorized access!' % response.status_code)
        return None
    elif response.status_code != 200:
        print('Status:', response.status_code, 'Problem with the getUserIdByEmail request.')
    else:
        data = response.json()
        user_list = data['users']
        for user in user_list:
            result = user['id']
        return result

def getOrgId(orgName):
    """ Finds given organization id """
    global username
    global pwd
    global BaseURL
    url = BaseURL+'/api/v2/organizations/autocomplete.json?name='+orgName
    result = ''

    response = requests.get(url, auth=(username, pwd))
    if response.status_code != 200:
        print('Status:', response.status_code, 'Problem with the getOrgId request.')
    else:
        data = response.json()
        org_list = data['organizations']
        if(len(org_list) > 0):
            for organization in org_list:
                if(orgName.upper() == organization['name'].upper()):
                    result = organization['id']
            return result
        #else:
           # print("%s organization not found" % orgName)

def deleteAllPhones(id):
    """ Deletes all given user phones """
    global username
    global pwd
    global BaseURL
    list_url = BaseURL+'/api/v2/users/' + str(id) + '/identities.json'
    response = requests.get(list_url, auth=(username, pwd))
    if response.status_code != 200:
        print('Status:', response.status_code, 'Problem with the deleteAllPhones request.')
    else:
        data = response.json()
        identity_list = data['identities']
        for identity in identity_list:
            if(identity['type'] == 'phone_number' or identity['type'] == 'phone'):
                identity_id = identity['id']
                del_url = BaseURL+'/api/v2/users/' + str(id) + '/identities/'+str(identity_id)+'.json'
                response = requests.delete(del_url,auth=(username, pwd))
                if response.status_code != 204:
                    print('Delete status:', response.status_code, 'Problem with the request.')

def numberToJsonFormat(tel_list):
    """ Converts list of phone numbers to json form """
    result = ''
    if(tel_list is not None):
        for id,num in enumerate(tel_list[::-1]):
            if(len(tel_list)-id > 1):
                result = result + '{"type": "phone_number", "value":"'+num+'"},'
            else:
                result = result + '{"type": "phone_number", "value":"'+num+'"}'
    ##print("["+result+"]")
    return "["+result+"]"


def updateUser(mail,fname,lname,org,title,tel_list,mail_list):
    """ Updates user given information """
    global username
    global pwd
    global BaseURL
    if(username == '' or pwd == '' or BaseURL == ''):
        raise ValueError('Username or password or baseURL field is empty!')
        exit()
    # User to update
    if(len(mail_list) == 0):
        id = getUserIdByEmail(mail)
    else:
        for m in mail_list:
            id = getUserIdByEmail(m)            
            if(id != '' and id is not None):
                 # Package the data in a dictionary matching the expected JSON
                data = ''
                name = fname + " " + lname
                if(name != '' and name is not None):
                    data = '{"user": {"name":"'+name+'"'
                    org = org
                    org_id = getOrgId(org)
                    if(org_id != '' and org_id is not None):
                        data = data + ',"organization_id":'+str(org_id)
                    occupation = title
                    if(occupation != '' and occupation is not None):
                        data = data + ',"user_fields": {"occupation":"'+occupation+'"}'
                    if(tel_list is not None and len(tel_list)>0):
                        deleteAllPhones(id)
                        data = data + ',"identities":'+ numberToJsonFormat(tel_list)+', "phone":"'+tel_list[0]+'"'

                data = data + '}}'
                # Encode the data to create a JSON payload
                payload = json.loads(data)
                payload = json.dumps(payload)
                # Set the request parameters
                url = BaseURL+'/api/v2/users/' + str(id) + '.json'
                headers = {'Content-Type': 'application/json'}
                # Do the HTTP put request
                response = requests.put(url, data=payload, auth=(username, pwd), headers=headers)
                # Check for HTTP codes other than 200
                if response.status_code != 200:
                    print('Status:', response.status_code, 'Problem with the updateUser request. Exiting.')
                else:   
                    # Report success
                    print('Successfully updated user %s (#%s) ' % (name, id))
                    return
    if(id is not None):
        print("User %s not found." % (fname + " " + lname))

#mail = "carmenwinter@arendus.ee"
#firstName = "Carmen"
#lastName = "Winter"
#organization = "Arendus OÜ"
#title = "CEO"
#telefon_list = ["+372 51 725 30", "+372 79 001 00"]
#mail_list = ["carmenwinter@arendus.ee","test@test.ee"]

#mail = "yajiaqi@yajiaqi.com"
#firstName = "Allen"
#lastName = "Chan"
#organization = "Organization\:"
#title = "EXPORT MANAGER"
#telefon_list = ['Wechat/Viber/Whatsapp', "Telephone: 0086-571-82187380"]
#mail_list = []
    
#updateUser(mail,firstName,lastName,organization,title,telefon_list,mail_list)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        updateUser(sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4],sys.argv[5],sys.argv[6],sys.argv[7])
    else:
        raise SystemExit("usage:  python userUpdater.py {email} {firstname} {lastname} {organization} {occupation} {phone_numbers} {mail_list}")
