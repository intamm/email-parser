#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Creates zendesk ticket and comments
#
import json
import requests

#################################GLOBAL VARIABLES##################################
###################################################################################

#Zendesk account information
username = ''
pwd = ''
#Zendesk base url - example: 'https://ticketsupport1.zendesk.com'
BaseURL= ''

####################################################################################
####################################################################################

class ticket:
    subject = ""
    comment = ""
    status = ""
    recipient = ""
    submitter_id = 0
    organization_id = 0
    tags = "import-2017"
    is_public = True
    created_at = ""
    updated_at = ""
    requester_id = 0

class comment:
    type = "Comment"
    body = ""
    plain_body = ""
    public = True
    author_id = 0
    created_at = 0

def createUser(emailAdr, name):
    url = BaseURL+"/api/v2/users.json"
    data = {"user":{"name":name, "email":emailAdr}}
    payload = json.dumps(data)
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, data=payload, auth=(username, pwd), headers=headers)
    if(response.status_code != 201):
        print('Status:', response.status_code, 'Problem with the createUser request.')
    else:
        print("Succesfully created User")

def getUserIdByData(data):
    url = BaseURL+"/api/v2/users.json"
    response = requests.get(url,auth=(username,pwd))
    result = ''
    emailAdr = data.submitter_id
    name = data.name
    if(response.status_code != 200):
        print('Status:', response.status_code, 'Problem with the getUserIdByEmail request.')
    else:
        data = response.json()
        user_data = data['users']
        for user in user_data:
            if(user['email'] == emailAdr):
                result = user['id']
    if(result == ''):
        result = createUser(emailAdr,name)
    return result

def getUserIdByEmail(emailAdr):
    url = BaseURL+"/api/v2/users.json"
    response = requests.get(url,auth=(username,pwd))
    result = ''
    if(response.status_code != 200):
        print('Status:', response.status_code, 'Problem with the getUserIdByEmail request.')
    else:
        data = response.json()
        user_data = data['users']
        for user in user_data:
            if(user['email'] == emailAdr):
                result = user['id']
    if(result == ''):
        result = createUser(emailAdr,emailAdr)
    return result

def getOrgId(id):
    url = BaseURL+"/api/v2/users/"+str(id)+".json"
    response = requests.get(url,auth=(username,pwd))
    result = 0
    if(response.status_code != 200):
        print('Status:', response.status_code, 'Problem with the getOrgId request.')
    else:
        data = response.json()
        user = data['user']
        result = user['organization_id']
    return result    

def getTicketId(id,subject):
    url = BaseURL+"/api/v2/tickets.json"
    response = requests.get(url,auth=(username,pwd))
    result = ''
    if(response.status_code != 200):
        print('Status:', response.status_code, 'Problem with the getTicketId request.')
    else:
        data = response.json()
        ticket_data = data['tickets']
        for ticket in ticket_data:
            if(ticket['requester_id'] == id and ticket['subject'].upper() == subject.upper()):
                result = ticket['id']
    return result    

def uploadTicket(newticket):
    data = {"ticket":{"subject":newticket.subject, "comment":{"body":newticket.comment}, "status":newticket.status, "recipient":newticket.recipient, "submitter_id":newticket.submitter_id, "organization_id":newticket.organization_id, "tags": [newticket.tags], "is_public":newticket.is_public, "created_at":newticket.created_at, "updated_at":newticket.updated_at, "requester_id":newticket.requester_id}} 
    payload = json.dumps(data)
    url = BaseURL+'/api/v2/tickets.json'
    headers = {'Content-Type': 'application/json'}
    response = requests.post(url, data=payload, auth=(username, pwd), headers=headers)
    if(response.status_code != 201):
        print('Status:', response.status_code, 'Problem with the uploadTicket request.')
    else:
        print("Succesfully uploaded Ticket '%s'" % newticket.subject)

def uploadComment(newComment, ticket_id):
    data = {"ticket": {"status":newComment.status, "comment": {"type":newComment.type, "body":newComment.body, "plain_body":newComment.plain_body, "public":newComment.public, "author_id":newComment.author_id, "created_at": newComment.created_at}}}
    payload = json.dumps(data)
    url = BaseURL+'/api/v2/tickets/'+str(ticket_id)+'.json'
    headers = {'Content-Type': 'application/json'}
    response = requests.put(url, data=payload, auth=(username, pwd), headers=headers)
    if(response.status_code != 200):
        print('Status:', response.status_code, 'Problem with the uploadComment request.')
    else:
        print("Succesfully uploaded Comment (#%s)" % ticket_id)    

def createTicket(data):
    newTicket = ticket()
    newTicket.subject = data.subject
    newTicket.comment = data.comment
    newTicket.status = data.status
    newTicket.recipient = data.submitter_id
    newTicket.submitter_id = getUserIdByData(data)
    newTicket.organization = getOrgId(newTicket.submitter_id)
    newTicket.is_public = data.is_public
    newTicket.created_at = newTicket.created_at
    newTicket.requester_id = newTicket.submitter_id
    uploadTicket(newTicket)

def createComment(data,orgMail):
    newComment = comment()
    newComment.body = data.comment
    newComment.plain_body = data.comment
    newComment.public = data.is_public
    newComment.author_id = getUserIdByData(data)
    newComment.created_at = data.created_at
    newComment.status = data.status
    cleanSubject = data.subject.upper().replace("RE: ",'').replace("FWD: ", '').replace("FW: ", '')
    oMail = getUserIdByEmail(orgMail)
    ticketId = getTicketId(oMail, cleanSubject)
    uploadComment(newComment, ticketId)
