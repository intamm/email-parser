#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Very basic using of Python 3 and IMAP to iterate over emails and parse them
#
#
import sys
import os
import imaplib
import getpass
import email
import email.header
import datetime
import base64
import talon
from talon.signature.bruteforce import extract_signature
from talon import quotations
import re
import vobject
import quopri
import html
import getpass
import userUpdater
import ticketCreator
import enum
talon.init()

############################################ GLOBAL VARIABLES #########################################################
#######################################################################################################################

# Use 'INBOX' to read inbox.
# Use '"[Gmail]/Sent Mail"' to read sent mail(WORKS ONLY IN GMAIL).
# after successfully running this script all emails in that folder will be marked as read.
# !!!!PUT INBOX ALWAYS IN FIRST POSITION!!!!
EMAIL_FOLDERS = ['INBOX']
EMAIL_ACCOUNT = ""
EMAIL_PASSWORD = ""
# IMAP server address:
try:
    M = imaplib.IMAP4_SSL('')
except ConnectionRefusedError:
    print("ERROR: Incorrect IMAP server address.")
    sys.exit(1)
# Use True to Find VCards and update Zendesk users, False to not find VCards or update users.
updateZendeskContacts = True
# Use True to get email texts, False to not get email texts.
email_text = False

#######################################################################################################################
#######################################################################################################################

class ticket:
    subject = ""
    comment = ""
    status = ""
    recipient = ""
    submitter_id = ""
    organization_id = ""
    tags = "import-2017"
    is_public = True
    created_at = 0
    updated_at = ""
    group = ""
    msg_id = ""
    ref = ""
    name = ""

    def __eq__(self,other):
        return self.msg_id==other.msg_id

    def __str__(self):
        return "SUBJECT: %s \nFROM: %s \nTO: %s \nGROUP: %s \nDATE: %s \nPUBLIC: %s \nSTATUS: %s \nMESSAGE: %s" % (self.subject, self.submitter_id, self.recipient, self.group, self.created_at, self.is_public, self.status, self.ref)

class vCard():
    firstName = ""
    lastName = ""
    org = ""
    title = ""
    mail = []
    phone = []
    orgMail = ""

    def __repr__(self):
        return "Name: %s %s, company: %s, role: %s, e-mail:%s, phone:%s" % (self.firstName, self.lastName, self.org, self.title, [x.value for x in self.mail], [y.value for y in self.phone])


# Global list for tickets
ticketList = []

#Global list for vCards
vCardList = []

# Common greetings and endings
greetingsList = ['Tere', 'Tere,', 'Tere!', 'Tervist', 'Tervist,', 'Tervist!', 'Hei', 'Hei,', 'Hei!']
endList = ['Parimat', 'Parimat,', 'Parimat.', 'Heade soovidega', 'Heade soovidega,', 'Heade soovidega.',
           'Äitah', 'Äitah,', 'Äitah!', 'Äitah.', 'Lugupidamisega', 'Lugupidamisega,', 'Lugupidamisega.',
           'Tervitades', 'Tervitades,', 'Tervitades!']


def createVCard(vc,orgMail):
    """
    Gets vCard components from input string.
    Returns created VCard.
    """
    newVCard = vCard()
    try:
        v = vobject.readOne(vc)
    except Exception as e:
        print("Incorrect vcard format. ERROR msg:", e)
        return ''
    newVCard.mail = []
    newVCard.phone = []
    newVCard.orgMail = orgMail
    try:
        firstName = v.n.value.given
        newVCard.firstName = firstName
    except:
        pass
    try:
        lastName = v.n.value.family
        newVCard.lastName = lastName
    except:
        pass
    try:
        mail = v.email_list
        for adr in mail:
            newVCard.mail.append(adr)
    except:
        pass
    try:
        organization = v.org.value
        newVCard.org = organization[0]
    except:
        pass
    try:
        organization = v.o.value
        newVCard.org = organization
    except:
        pass
    try:
        title = v.title.value
        newVCard.title = title
    except:
        pass
    try:
        telefon = v.tel_list
        for num in telefon:
            newVCard.phone.append(num)
    except:
        pass
    return newVCard

def domainsplit(x):
    """
    Splits domain address from email
    Returns domain address.
    """
    try:
        return x.split('@')[1]
    except:
        return 'Could not find a domain'

def checkReply(subject, mail):
    """ Checks if email is reply or forwarded, if it is, finds original message-id """
    result = ''
    if ("Re: " in subject or "Fwd: " in subject or "Fw: " in subject or "FW: " in subject or "RE: " in subject or "Vs: " in subject):
        subject = subject.replace("Re: ",'').replace("Fwd: ", '').replace("Fw: ", '').replace("FW: ", '').replace("RE: ", '')
        for x in sorted(ticketList, key=lambda ticket: (datetime.datetime.strptime(ticket.created_at, '%a, %d %b %Y %H:%M:%S'))):
            if(x.subject == subject and x.submitter_id == mail):
                result = x.msg_id      
    return result
        

def removeNoise(text, name, mail):
    """Removes possible signature and forwarded messages"""

    arr = text.split('\n')
    newArr = [n.strip() for n in arr]
    for idx, n in enumerate(newArr[:]):
        if('Forwarded message' in n.strip()):
            if(len(newArr) > len(arr[0:idx])):
                newArr = arr[0:idx]
        if('Reply to message' in n.strip()):
            if(len(newArr) > len(arr[0:idx])):
                newArr = arr[0:idx]
        for i in greetingsList:
            if(i in newArr and n in newArr):
                newArr.remove(n)
        for j in endList:
            for ids, k in enumerate(newArr):
                if(ids > 1 and j == k):
                    newArr = newArr[0:ids]
    text = '\n'.join(newArr)

    if(name in text):
        text = text.replace(name,'')
    if(mail in text):
        text = text.replace(mail,'')
    if("--" in text):
        text = text.replace("--",'')
    if('&nbsp;' in text):
        text = text.replace('&nbsp;',' ')
    if('Today,' in text):
        ind = re.search(r'\b(Today)\b',text)
        text = text[0:ind.start()]
    return text.strip()

def getId(string):
    """Returns cleaned message-id"""
    for idx, s in enumerate(string):
        if(s == '@'):
            string = string[1:idx]
    return string

def removeGmailLine(string):
    """
    Removes Estonian gmail reply line
    Exmpl: 10. november 2017 14:19 kirjutas Peeter Paan <peeter_paan@gmail.com>:
    """
    arr = string.split('\n')
    for n in arr:
        if('kirjutas' in n and '@' in n):
            arr.remove(n)
    return '\n'.join(arr)

def payloadDecoder(payload):
    """
    Decodes payload with correct charset to string
    Returns decoded string.
    """
    if(payload.get_payload(decode=True) is not None and payload is not None and payload.get_payload() is not None):
        encoding = payload.get_content_charset()
        if encoding is None:
            try:
                encoding = 'ISO-8859-1'
                result = payload.get_payload(decode=True).decode(encoding)
            except:
                result = payload.get_payload()
        elif(encoding.upper() == 'US-ASCII' or encoding.upper() == 'ISO-8859-1'):
            encoding = 'iso-8859-1'
            result = payload.get_payload(decode=True).decode(encoding)
        else:
            try:
                decoded = payload.get_payload(decode=True).decode(encoding)
                result = (quopri.decodestring(decoded))
                result = result.decode(encoding)
            except:
                try:
                    result = payload.get_payload(decode=True).decode(encoding)
                except:
                    try:
                        result = payload.get_payload(decode=True).decode(encoding, errors='ignore')
                    except:
                        return ''                      
        return result
    else:
        return ''
def updateZendeskUser(vcard):
    """
    Updates existing user in zendesk
    """
    if(type(vcard) is vCard):
        print("Found VCard: ",vcard)
        userUpdater.updateUser(vcard.orgMail,vcard.firstName,vcard.lastName,vcard.org,vcard.title,[y.value for y in vcard.phone], [x.value for x in vcard.mail])
        print()

def updateTicketsAndComments():
    for tickets in sorted(ticketList, key=lambda ticket: (ticket.group, datetime.datetime.strptime(ticket.created_at, '%a, %d %b %Y %H:%M:%S'))):
        if(tickets.group == tickets.msg_id):
            ticketCreator.createTicket(tickets)
        else:
            ticketCreator.createComment(tickets,getOriginalMail(tickets.group)) 

def sortAndPrintTickets():
    """
    Adds group to ticket and then
    sorts tickets by group and by created at date.
    Finally prints them out.
    """
    for x in sorted(ticketList, key=lambda ticket: (datetime.datetime.strptime(ticket.created_at, '%a, %d %b %Y %H:%M:%S'))):
        x.group = getGroup(x.subject,x.ref,x.msg_id,x.submitter_id)
        
    #checks if comment is public or not    
    isPublic()
    
    group = ''
    for tickets in sorted(ticketList, key=lambda ticket: (ticket.group, datetime.datetime.strptime(ticket.created_at, '%a, %d %b %Y %H:%M:%S'))):
        if(tickets.group != group):
            try:
                group = tickets.group
                tickets.status = "new"
      #          print("==================================N-E-W==G-R-O-U-P====================================")
     #           print(tickets)
            except:
                print("ERROR with:",tickets.msg_id)
        else:
            tickets.status = getStatus(tickets)
    #        print(tickets)
   #     print ("--------------------------------------------------------------------------------------")
  #  print ("======================================================================================")

#Regex for seperating email address from text
regex = re.compile(("([a-z0-9!#$%&*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`"
                    "{|}~-]+)*(@|\sat\s)(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?(\.|"
                    "\sdot\s))+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)"))

def get_emails(s):
    """Returns an iterator of matched emails found in string s."""
    # Removing lines that start with '//' because the regular expression
    # mistakenly matches patterns like 'http://foo@bar.com' as '//foo@bar.com'.
    return (email[0] for email in re.findall(regex, str(s)) if not email[0].startswith('//'))

def getOriginalMail(ref):
    """
    Returns original email address.
    """
    result = ''
    for x in sorted(ticketList, key=lambda ticket: (datetime.datetime.strptime(ticket.created_at, '%a, %d %b %Y %H:%M:%S'))):
        if(x.msg_id == ref):
            result = x.submitter_id

    return result

def getStatus(tickets):
    """
    Checks if status is new/open/pending/solved
    Returns status
    """
    FromAdr = tickets.submitter_id
    ToAdr = tickets.recipient
    OrgAdr = getOriginalMail(tickets.group)
    status = "unkown"
    if(ToAdr == EMAIL_ACCOUNT and tickets.ref != '' and tickets.ref is not None and tickets.is_public == True):
        status = "open"
    elif(FromAdr == EMAIL_ACCOUNT and tickets.is_public == True):
        status = "pending"
    else:
        status  = "pending"
    
    return status

def getCommentType(From,To,OrgMail):
    """
    Defines if comment is public or not.
    Returns type - [True/False]
    """
    if(OrgMail == From or OrgMail == To):
        type = True
    elif(OrgMail != From and OrgMail != To):
        type = False
    else:
        type = True
    return type

def isPublic():
    """
    iterates through ticketList and corrects comment public status.
    """
    for x in sorted(ticketList, key=lambda ticket: (datetime.datetime.strptime(ticket.created_at, '%a, %d %b %Y %H:%M:%S'))):
        x.is_public = getCommentType(x.submitter_id,x.recipient,getOriginalMail(x.group))
      #  x.status = getStatus(x.subject,x.submitter_id,x.recipient,getOriginalMail(x.group))

def getGroup(subject,ref,msgid,mail):
    """
    Finds ticket group
    Returns group as string
    """
    result = ''
    global ticketList
    if ref is None:
        if ("Fwd: " in subject or "Fw: " in subject or "Re: " in subject or "FW: " in subject or "RE: " in subject or "Vs: " in subject):
            tmp = checkReply(subject,mail)
            if(tmp == '' or tmp is None):
                result = msgid
            else:
                result = checkReply(subject,mail)
        else:
            result = msgid.strip()
    else:
        tmpArr = re.split(' |,|\n',ref)
        if(len(tmpArr) > 1):
            for i,n in enumerate(tmpArr):
                obj = next((x for x in ticketList if x.msg_id == tmpArr[i].strip()), None)
                if(obj is not None):
                   result = obj.group
                   break
                else:
                    result = msgid.strip()
        else:
            obj = next((x for x in ticketList if x.msg_id == ref), None)
            if(obj is not None):
                result = obj.group
            else:
                result = msgid.strip()
    return result

def process_mailbox(M):
    """
    Gets emails via IMAP 
    """
    rv, data = M.search(None, "ALL")
   # rv, data = M.search(None, '(SINCE "01-Nov-2017")')
    if rv != 'OK':
        print("No messages found!")
        return
    count = 0
    for num in data[0].split():
        rv, data = M.fetch(num, '(RFC822)')
        if rv != 'OK' or len(data) < 2:
            print("ERROR getting message", num)
        else:
            msg = email.message_from_bytes(data[0][1])
            try:
                hdr = email.header.make_header(email.header.decode_header(str(msg['Subject'])))
                subject = str(hdr)
            except:
                hdr = email.header.decode_header(str(msg['Subject']))
                if(type(hdr[0][0]) is 'bytes' and hdr[0][1] is not None):
                    subject = hdr[0][0].decode(hdr[0][1], errors="ignore")
                else:
                    subject = str(hdr[0][0])            
            domain = domainsplit(email.utils.parseaddr(str(msg['From']))[1])
            toList = get_emails(msg['to'])
            mail = email.utils.parseaddr(str(msg['From']))[1]
            name = email.utils.parseaddr(str(msg['From']))[0]
            msg_id = msg['Message-id']
            ref = msg['References']
            inrepto = msg['In-Reply-To']
            mailto = email.utils.parseaddr(str(msg['To']))[1]
            date = email.utils.parsedate_tz(msg['Date'])
            n, m = email.header.decode_header(name)[0]
            if(m is None):
                decodedName = name
            else:
                decodedName = n.decode(m)
          #  print('ID: ', msg_id)            
          #  print('Subject: ', subject.encode(errors="ignore"))
          #  print('From: ', mail)
          #  print('From name: ', decodedName)
          #  print('From domain: ', domain)
          #  print('To: ', mailto)
          #  print('Ref: ',ref)
          #  print('In-Reply-to: ', inrepto)
          
            # Now convert to local date-time
            date_tuple = date
            if date_tuple:
                local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(date_tuple))
              #  print ("Recieved/Sent Date:", local_date.strftime("%a, %d %b %Y %H:%M:%S"))
            createDate = local_date.strftime("%a, %d %b %Y %H:%M:%S")
            print("Processing e-mail #%s, sender:%s, subject %s" % (str(count),mail,subject.encode(errors="ignore")))      
            foundText = False
            foundVCard = False
            for part in msg.walk():
                if part.is_multipart():
                    for payload in part.get_payload():
                       ctype = payload.get_content_type()
                       cdispo = str(payload.get('Content-Disposition'))
                       _str = payloadDecoder(payload)
                       if (ctype == 'text/plain' and 'attachment' not in cdispo and email_text is True):
                           res = ''
                           text, signature = extract_signature(_str.replace('&lt;','<').replace('&gt;','>'))
                           reply = quotations.extract_from(text, 'text/plain')
                           reply = quotations.extract_from_plain(text)
                           res = removeNoise(removeGmailLine(reply),name,mail)
                           foundText = True
                       if (ctype == 'text/html' and 'attachment' not in cdispo and foundText is False and email_text is True):
                           res = ''
                           htmlString = re.sub('<[^<]+?>', '',html.unescape(_str))
                           text, signature = extract_signature(_str.replace('&lt;','<').replace('&gt;','>'))
                           reply = quotations.extract_from(text, 'text/plain')
                           reply = quotations.extract_from_plain(text)
                           res = removeNoise(removeGmailLine(reply),name,mail)
                           foundText = True
                       if(payload.get_content_subtype() == 'x-vcard' and updateZendeskContacts is True and folder == EMAIL_FOLDERS[0] and foundVCard is False):
                          updateZendeskUser(createVCard(_str,mail))
                          foundVCard = True
                       if(payload.get_content_subtype() == 'vcard' and updateZendeskContacts is True and folder == EMAIL_FOLDERS[0] and foundVCard is False):
                          updateZendeskUser(createVCard(_str,mail))
                       if(ctype == 'text/directory' and updateZendeskContacts is True and folder == EMAIL_FOLDERS[0] and foundVCard is False):
                          updateZendeskUser(createVCard(_str,mail))
                elif(part.is_multipart() == False and foundText == False):
                    ctype = part.get_content_type()
                    cdispo = str(part.get('Content-Disposition'))
                    _str = payloadDecoder(part)
                    if(ctype == 'text/plain' and 'attachment' not in cdispo and email_text is True):
                        res = ''
                        text, signature = extract_signature(_str.replace('&lt;','<').replace('&gt;','>'))
                        reply = quotations.extract_from(text, 'text/plain')
                        reply = quotations.extract_from_plain(text)
                        res = removeNoise(removeGmailLine(reply),name,mail)
                        foundText = True
                    if(ctype == 'text/html' and 'attachment' not in cdispo and foundText is False and email_text is True):
                        res = ''
                        htmlString = re.sub('<[^<]+?>', '',html.unescape(_str))
                        text, signature = extract_signature(_str.replace('&lt;','<').replace('&gt;','>'))
                        reply = quotations.extract_from(text, 'text/plain')
                        reply = quotations.extract_from_plain(text)
                        res = removeNoise(removeGmailLine(reply),name,mail)
                        foundText = True
                    if(part.get_content_subtype() == 'x-vcard' and updateZendeskContacts is True and folder == EMAIL_FOLDERS[0] and foundVCard is False):
                        updateZendeskUser(createVCard(_str,mail))
                        foundVCard = True
                    if(part.get_content_subtype() == 'vcard' and updateZendeskContacts is True and folder == EMAIL_FOLDERS[0] and foundVCard is False):
                        updateZendeskUser(createVCard(_str,mail))
                        foundVCard = True
                    if(ctype == 'text/directory' and updateZendeskContacts is True and folder == EMAIL_FOLDERS[0] and foundVCard is False):
                        updateZendeskUser(createVCard(_str,mail))
                        foundVCard = True
        if(email_text is True):   
            newTicket = ticket()
            newTicket.subject = subject
            newTicket.comment = res
            res = ''
            newTicket.recipient = mailto
            newTicket.created_at = createDate
            newTicket.updated_at = date
            newTicket.submitter_id = mail
            newTicket.msg_id = msg_id
            newTicket.ref = ref
            newTicket.name = name
            global ticketList
            if not(newTicket in ticketList):
                ticketList.append(newTicket)
        count=count+1
        
try:
    rv, data = M.login(EMAIL_ACCOUNT, EMAIL_PASSWORD)
except imaplib.IMAP4.error:
    print ("LOGIN FAILED! Check your username and password.")
    sys.exit(1)

print(rv, data)

# UNCOMMENT THIS SECTION TO FIND OUT MAILBOXES NAMES:
#rv, mailboxes = M.list()
#if rv == 'OK':
#    print("Mailboxes:")
#    print(mailboxes)

for folder in EMAIL_FOLDERS:
    if(email_text is False and updateZendeskContacts is False):
        print("Either 'email_text' or 'updateZendeskContacts' has to equal True")
        break
    elif(email_text is False and updateZendeskContacts is True):
        print (folder, "response:",M.select(folder)[0])
        rv, data = M.select(folder)
        if rv == 'OK':
            print("Processing "+ folder +"...\n")
            process_mailbox(M)
            M.close()
            print("Processing done!")
            print("======================================================")
            break
        else:
            print("ERROR: Unable to open mailbox ", rv)
    else:
        print (folder, "response:",M.select(folder)[0])
        rv, data = M.select(folder)
        if rv == 'OK':
            print("Processing "+ folder +"...\n")
            process_mailbox(M)
            M.close()
            print("Processing done!")
            print("======================================================")
        else:
            print("ERROR: Unable to open mailbox ", rv)
if(email_text is True):
    sortAndPrintTickets()
    updateTicketsAndComments()
M.logout()
