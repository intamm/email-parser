#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Updates given organization external_id
#
import re
import json
import requests

#Zendesk account information
username = ''
pwd = ''
#Zendesk base url - example: 'https://ticketsupport1.zendesk.com'
BaseURL= ''

def updateAllExternalIds():
    global username
    global pwd
    global BaseURL
    url = BaseURL + '/api/v2/organizations.json'

def update(id,external_id):
    global username
    global pwd
    global BaseURL
    data = {"organization":{"external_id":external_id}}
    payload = json.dumps(data)
    url = BaseURL+'/api/v2/organizations/'+str(id)+'.json'
    print(url)
    headers = {'Content-Type': 'application/json'}
    response = requests.put(url, data=payload, auth=(username, pwd), headers=headers)
    if response.status_code != 200:
        print('Status:', response.status_code, 'Problem with the update request.')
    else:
        print('Succesfully added external_id to #%s!' % id)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        updateUser(sys.argv[1],sys.argv[2])
    else:
        raise SystemExit("usage:  python setExternal_id.py {organization_id} {reg. code}")
