#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Updates Zendesk organization names and domains.
#
import re
import json
import requests
import datetime
import email
from urllib import parse

######################## GLOBAL VARIABLES #########################
####################################################################

#Zendesk account information
username = ''
pwd = ''
#Zendesk base url - example: 'https://ticketsupport1.zendesk.com'
baseURL= ''

#GRAPH-API account information
usr = ''
pw = ''

#####################################################################
#####################################################################

def getLegalName(orgID,updated_at):
    #GRAPH-API information
    url = "https://graph.ir.ee/organizations/ee-"+str(orgID)+".json"
    global usr
    global pw
    result = ''
    response = requests.get(url,auth=(usr,pw))
    if(response.status_code == 204):
        print("Organization with reg. code:%s not found!" % orgID)
        return
    elif(response.status_code != 200):
       print('Status:', response.status_code,response.reason+'.', 'Problem with the getDomainName request.')
    else:
       data = response.json()
       org_data = data['result']
       for info in org_data.items():
           if(info[0] == 'primaryTopic'):
               result = info[1]['legalName']

    return result

def updateName(newName, orgId):
    global username
    global pwd
    global baseURL
    data = {"organization":{"name":newName}}
    payload = json.dumps(data)
    url = baseURL+'/api/v2/organizations/'+str(orgId)+'.json'
    headers = {'Content-Type': 'application/json'}
    response = requests.put(url, data=payload, auth=(username, pwd), headers=headers)
    if(response.status_code != 200):
        return False
        print('Status:', response.status_code, 'Problem with the updateDomainName request.')
    else:
        return True
        print('Succesfully updated organization %s (#%s) ' %(newName,orgId))

def getEmailbyURL(orgID):
    #GRAPH-API information
    url = "https://graph.ir.ee/organizations/ee-"+str(orgID)+"/urls"
    global usr
    global pw
    result = []
    response = requests.get(url,auth=(usr,pw))
    if(response.status_code != 200):
       print('Status:', response.status_code, 'Problem with the getEmailbyURL request.')
    else:
        data = response.json()
        org_data = data['result']
        for info in org_data.items():
            if(info[0] == 'items'):
                length = len(info[1])
                if(length != 0):
                    for x in range(0,length):
                        url = info[1][x]['hasValue']
                        result.append(parse.unquote(url.replace('http://www.','').replace('https://www.','').replace('www.','').replace('http://','').replace('https://','')))
    return result


def getEmail(orgID):
    #GRAPH-API information
    url = "https://graph.ir.ee/organizations/ee-"+str(orgID)+"/emails"
    global usr
    global pw
    result = []
    response = requests.get(url,auth=(usr,pw))
    if(response.status_code != 200):
       print('Status:', response.status_code, 'Problem with the getEmail request.')
    else:
        data = response.json()
        org_data = data['result']
        for info in org_data.items():
            if(info[0] == 'items'):
                length = len(info[1])
                if(length != 0):
                    for x in range(0,length):
                        value = info[1][x]['hasValue']
                        result.append(value.replace('mailto:',''))
    return result

def emailstoJsonFormat(email_list):
    result = ''
    if(email_list is not None and len(email_list) != 0):
        for id,email in enumerate(email_list[::1]):
            if(id == 0):
                result = result + '{"organization":{"domain_names":["'+email+'"'
            else:
                result = result + ',"'+email+'"'
    return result+"]}}"

def updateEmail(email_list,id):
    global username
    global pwd
    global baseURL
    data = emailstoJsonFormat(email_list)
    payload = json.loads(data)
    payload = json.dumps(payload)
    url = baseURL+'/api/v2/organizations/'+str(id)+'.json'
    headers = {'Content-Type': 'application/json'}
    response = requests.put(url, data=payload, auth=(username, pwd), headers=headers)
    if(response.status_code != 200):
        print('Status:', response.status_code, 'Problem with the updateDomainName request.')
        return False
    else:
        return True
        print('Succesfully updated organizations (#%s) emails' %(id))

def domainsplit(x):
    """
    Splits domain address from email
    Returns domain address.
    """
    try:
        return x.split('@')[1]
    except:
        return 'Could not find a domain'

def splitDomainsFromList(lst):
    newList = list(set([domainsplit(x) for x in lst]))
    return newList

def updateAllNamesAndDomains():
    global username
    global pwd
    global baseURL
    nameResult = False
    emailResult = False
    fstNewEmail = []
    sndNewEmail = []
    url = baseURL + '/api/v2/organizations.json'
    response = requests.get(url,auth=(username,pwd))
    if response.status_code != 200:
        print('Status:', response.status_code, 'Problem with the updateAllNames request.')
    else:
        data = response.json()
        org_list = data['organizations']
        for org in org_list:
            org_external_id = org['external_id']
            org_id = org['id']
            currentName = org['name']
            updated_at = org['updated_at']
            newName = getLegalName(org_external_id,updated_at)
            if(newName is not None and newName != ''):
                if(currentName.upper() != newName.upper() and newName != ''):
                    nameResult = updateName(newName,org_id)
                domain_list = org['domain_names']
                fstNewEmail = splitDomainsFromList(getEmail(org_external_id))
                sndNewEmail = getEmailbyURL(org_external_id)
                mergeList = list(set(fstNewEmail+sndNewEmail))
                updateEmails = False
                for domain in domain_list:
                    if(domain not in mergeList):
                        mergeList.append(domain)
                        updateEmails = True                 
                if(updateEmails is True):                
                    emailResult = updateEmail(mergeList,org_id)
                print("Succesfully updated: Organization %s(#%s) name:%s, domains:%s" % (newName,org_id,nameResult,emailResult))
            
#getLegalName("14329294")
#print(getEmailbyURL("10000018"))
#getDomainName("10000018")
#getEmailbyURL('12203636')
#print(getEmail('10000018'))
updateAllNamesAndDomains()
